### 👋 hi, i'm hayden

oh no how do i finish projects

#### 💎 now

i'm working on these public projects right now
- [rust-fundamentals](https://gitlab.com/htbrown/rust-fundamentals)

#### 📺 projects

these projects are cool
- [htbrown.com](https://gitlab.com/htbrown/htbrown.com)
- [dotfiles](https://gitlab.com/htbrown/dotfiles)
- [project-euler-python](https://gitlab.com/htbrown/project-euler-python)
- [jive](https://gitlab.com/htbrown/jive)
- [hfetch](https://gitlab.com/htbrown/hfetch)

also, if you want, some of my work from [university and school](https://gitlab.com/htbrown-edu) is available to see

#### [htbrown.com](https://htbrown.com)